import { BotFactory } from '../src/Types.d.ts'

const bot: BotFactory = () => {
	let theirLastMove = ''
	let lastResult = 0

	// Called after each round with the results
	// result is 1 for win, -1 for loss, 0 for tie
	const Report = (myMove: string, theirMove: string, result: int) => {
		lastResult = result
		theirLastMove = theirMove
	}

	const Shoot = () => {
		// Decide what to do
		return theirLastMove || 'rock'
	}

	return {
		Name: 'Name of my Bot',
		Shoot,
		Report,
	}
}

export default bot
