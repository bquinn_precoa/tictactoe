import Cycler from './bots/example/Cycler.ts'
import Bot from 'https://gitlab.com/bquinn_precoa/rock-paper-scissors/-/raw/main/bots/Bot.ts'
import Random from './bots/example/Random.ts'

import { roundRobin } from './src/RockPaperScissors.ts'

const bots = [
	Cycler,
	Bot,
	Random,
]

roundRobin(bots, 3)
